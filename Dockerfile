FROM alpine:3.14

COPY entrypoint.sh /

ENTRYPOINT [ "/entrypoint.sh" ]
